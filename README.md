Задача используя Ansible установить на три удаленных машины установить:
1 Zabbix-server с PostgreSQL в режиме Master
2 Zabbix-agent, для работы с Zabbix-server
3 PostgreSQL в режиме Slave для репликации с Master

IP адреса машин
1 10.3.2.211
2 10.3.2.212
3 10.3.2.213

Машина с Ansible
4 10.3.2.214

Для создания ролей использовалась команда
ansible-galaxy init <Имя роли>

Для шифрования переменных использовалась команда
echo -n "Переменная" | ansible-vault encrypt_string

Для запуска используется команда
ansible-playbook <Имя плейбука>.yml --ask-pass --ask-vault-pass

В трех директориях находятся плейбуки для инсталляции.
Для простоты создданы скрипты запуска плейбуков bash
./go


